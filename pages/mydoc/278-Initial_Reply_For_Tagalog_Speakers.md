---
title: Initial Reply - For Tagalog Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Tagalog speakers"
sidebar: mydoc_sidebar
permalink: 278-Initial_Reply_For_Tagalog_Speakers.html
folder: mydoc
conf: Public
lang: fl
---


# Initial Reply - For Tagalog Speakers
## First response, Email to Client for Tagalog speakers

### Body


Mabuhay {{ beneficiary name }},

Salamat sa inyong pagdulog sa Access Now Digital Security Helpline (https://www.accessnow.org/help), ako nga pala si {{ incident handler name }} at ako'y narito upang kayo ay tulungan.

Natanggap na namin ang inyong panimulang mensahe at amin na itong pinupuproseso.

Para sa inyong kaalaman ang Helpline ay binubuo ng mga security professional na nakikipagugnayan mula iba't-ibang parte ng mundo. Dahil dito, maaaring ibang miyembro ng aming grupo and sumagot sa inyong katanungan. Depende ito sa oras at araw na aming natanggap ang inyong mensahe. Ako, o isa sa aking mga kasamahan ang makikipag-usap sa inyo sa madaling panahon upang magtanong ng karagdagang impormasyon na kinakailangan para maisagawa ang inyong mungkahi.

Maaari lamang pong ipagpatuloy niyong ilagay ang "[accessnow #{{ ticket id }}]" sa subject ng mga susunod niyong mensahe patungkol sa inyong probelama.

Lubos na bumabati,

{{ incident handler name }}


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
