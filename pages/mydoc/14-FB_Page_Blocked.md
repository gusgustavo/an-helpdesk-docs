---
title: Facebook - Page Blocked
keywords: email templates, page recovery, Facebook
last_updated: August 12, 2021
tags: [account_recovery_templates, templates]
summary: "Initial email to client about a Facebook page blocked due to an alias"
sidebar: mydoc_sidebar
permalink: 14-FB_Page_Blocked.html
folder: mydoc
conf: Public
ref: FB_Page_Blocked
lang: en
---


# Facebook - Page Blocked
## Initial email to client about a Facebook page blocked due to an alias

### Body

Hi {{ beneficiary name }},

My name is {{ incident handler name }} from the Access Now Digital Security Helpline, and we are happy to help you.

I have read your request, and in this particular case we need to know if you have already tried to change your name to the original one or to revoke the suspension of your account through the official Facebook form: https://www.facebook.com/help/contact/260749603972907

We need to know this to decide which is the best way to proceed.

Thank you,

{{ incident handler name }}
