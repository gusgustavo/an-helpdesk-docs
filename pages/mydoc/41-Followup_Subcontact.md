---
title: Follow-up - Subcontact
keywords: follow-up, referrals, third-party consultants, email templates
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Email to client to follow up on a referral to a third party"
sidebar: mydoc_sidebar
permalink: 41-Followup_Subcontact.html
folder: mydoc
conf: Public
ref: Followup_Subcontact
lang: en
---


# Follow-up - Subcontact
## Email to client to follow up on a referral to a third party

### Body

Hi {{ beneficiary name }},

I'm {{ incident handler name }} from the Digital Security Helpline at Access Now.

Right now I'm doing a follow up on this case. Could you let us know if you succeeded in contacting {{ contact's name }}, the person who [[ details on what this person had to do ]]?

Thanks,

{{ incident handler name }}
