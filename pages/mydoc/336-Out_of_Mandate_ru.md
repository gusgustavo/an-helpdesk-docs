---
title: Out of Mandate - Email Template - Russian
keywords: email templates, mandate check policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Письмо клиенту о том, что его запрос вне нашей компетенции"
sidebar: mydoc_sidebar
permalink: 336-Out_of_Mandate_ru.html
folder: mydoc
conf: Public
ref: Out_of_Mandate
lang: ru
---


# Out of Mandate - Email Template
## Email to client to communicate that their request is out of mandate

### Body

[[ ЕСЛИ ЗАПРОС НАХОДИТСЯ ВНЕ НАШЕЙ КОМПЕТЕНЦИИ, ПОТОМУ ЧТО ХАРАССМЕНТ ИМЕЕТ ЧАСТНЫЙ ХАРАКТЕР, ПОЖАЛУЙСТА, ВНИМАТЕЛЬНО ПРОЧТИТЕ [Article #234: Online Harassment Targeting a Civil Society Member](234-Online_Harassment_Against_Civil_Society_Member.html) И ДОБАВЬТЕ ПОДХОДЯЩИЕ ССЫЛКИ/РЕСУРСЫ В ЭТО ПИСЬМО. ЕСЛИ ЖЕРТВА НЕ ЖЕЛАЕТ СООБЩАТЬ О ПРОИЗОШЕДШЕМ ВЛАСТЯМ, МЫ НЕ ДОЛЖНЫ СВЯЗЫВАТЬСЯ С ПОЛИЦИЕЙ ]]

Здравствуйте,

Меня зовут {{ incident handler name }}. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now (https://www.accessnow.org/help). Спасибо, что обратились к нам.

Мы понимаем, что вы оказались в сложной ситуации. К сожалению, такого рода случаи находятся вне компетенции нашей Службы поддержки. [[ ПРИЧИНА, ПО КОТОРОЙ ЭТО ДЕЛО ОКАЗЫВАЕТСЯ ВНЕ КОМПЕТЕНЦИИ ]]

Вот некоторые онлайновые ресурсы, которые в данном случае могут оказаться полезны:

[[ ЕСЛИ ПРОБЛЕМА СВЯЗАНА С СОЦИАЛЬНЫМИ СЕТЯМИ ]]
- Как повысить безопасность в социальных сетях:
    - https://iheartmob.org/resources/safety_guides
    - https://safe.roskomsvoboda.org/socialnetworks/

[[ ОБЩИЕ СОВЕТЫ ПО БЕЗОПАСНОСТИ В СЛУЧАЕ ХАРАССМЕНТА ]]
- Советы по безопасности в сети:
    - http://www.fightcyberstalking.org/online-safety-tips/

[[ СОВЕТЫ О ПАРОЛЯХ ]]
- Как повысить безопасность паролей:
    - https://ssd.eff.org/ru/module/%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0%D0%B4%D1%91%D0%B6%D0%BD%D1%8B%D1%85-%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9

[[ ЕСЛИ ЕСТЬ ФИЗИЧЕСКАЯ УГРОЗА ]]
Если у вас есть причины полагать, что существует угроза вашей физической безопасности, пожалуйста, примите скорейшие меры, включая обращение в правоохранительные органы (если это уместно).

Возможно, есть смысл обратиться к организациям из списка ниже и попробовать получить реальную помощь в сложившейся ситуации.

[[ НАЙТИ МЕСТНЫЕ ГОРЯЧИЕ ЛИНИИ ПОДДЕРЖКИ ИЛИ КОНТАКТЫ ПОЛИЦИИ В СЛУЧАЕ КИБЕРПРЕСЛЕДОВАНИЯ, А ТАКЖЕ РЕГИОНАЛЬНЫЕ И НАЦИОНАЛЬНЫЕ ОРГАНИЗАЦИИ, РАБОТАЮЩИЕ С ТАКИМИ ПРОБЛЕМАМИ ]].

С уважением,

{{ incident handler name }}


* * *


### Related Articles

- [Article #234: Online Harassment Targeting a Civil Society Member](234-Online_Harassment_Against_Civil_Society_Member)
