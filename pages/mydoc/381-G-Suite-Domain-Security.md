---
title: G-Suite Domain Security Review & Hardening
keywords: G-Suite, Google, Domain, Organizational Security
last_updated: May 20, 2020
tags: [infrastructure, articles]
summary: "Client organization has concerns or is experiencing issues regarding digital security when using G-Suite, and asks for help to review it and possibly improve its configuration"
sidebar: mydoc_sidebar
permalink: 381-G-Suite-Domain-Security.html
folder: mydoc
conf: Public
lang: en
---


# G Suite Security Review & Hardening
## Essential parameters to review and configure to harden a G-Suite domain
### Problem

- The client organization has experienced security incidents related to their G-Suite identities, emails, file shares, applications, or data.
- The client organization wishes to review their G-Suite accounts for indicators of compromise.
- The client organization wishes to implement best practices for securing and hardening its G-Suite domain.

Note: This article assumes the client organization is using G-Suite Basic, Non-Profit, or Education edition. Clients with G-Suite for Enterprise will have additional security features such as the [G-Suite Security Center](https://gsuite.google.com/products/admin/security-center/).

### Solution

The client should conduct a review of their G-Suite administrative settings and implement best practices tailored to their context and their organizational capacity. This article will review the basic configurations and checks which will be appropriate for most organizations using G-Suite for their communications and productivity suite.

Depending on the capabilities of the client and the time available to dedicate to assisting the client, this process may be carried out by:

- Scheduling a session with the client, in which they would share their screen with you and you walk them through the various checks.
- Sending the client a set of instructions to follow in order to check and report back to you their current configuration. This would subsequently allow you to provide advice on appropriate next steps.
- Referring the client to this article, to follow it and implement its recommendations independently.


#### G-Suite Admin Console

The client must have Super Admin role in G-Suite to carry out all the steps and changes. To confirm they have this role they should log in to the Admin Console at [https://admin.google.com](https://admin.google.com). Once logged in, they should see around 15 options in the Console, including 'Devices', 'Admin Roles', 'Domains', and 'Data Migration'. If they see fewer options, they may have a delegated admin role and will not be able to carry out all the steps included in this article.

Oftentimes organizations will contract a local service provider to configure their G-Suite domain and make payments to Google on their behalf. In cases where the service provider retains Super Admin access to the organization's G-Suite domain, the client should understand that this introduces risk of external access and interference by the service provider. The organization should take an informed decision about who will have administrative access to the domain, in order to ensure security and control of it, as well as reduce the risk of being locked out or having it mismanaged.

#### Checking Vs. Changing

The client may make recommended changes immediately or may note them to implement later after further researching the details of the configurations, to carry out internal change management processes, or to give notice to their users about the consequences and necessary actions related to the changes. Whether a change is made immediately or not will depend on the nature of the change, the technical confidence and official role/mandate of the client, the potential of unintended consequences, the need for user interaction or preparation, and the complexity of the change itself.

#### User Review

The first step is to review the user accounts which exist within the domain, to ensure that there are no unknown/unwanted accounts.

1. From the Admin Console click **Users**.
2. Browse through the users displayed here (there may be multiple pages of them). Check that there are no unexpected user accounts present. You might check the 'Last Sign In' column to look either for accounts which are no longer used or dormant and could instead be suspended, or accounts which should no longer be used but have been accessed.
3. If accounts are deleted, you may be able to reduce the number of active licenses you purchase from Google.

Next, check which users have administrative roles:

1. From the same Users page, click **Add a filter** and select **Admin Role**, then check both _Super Admin_ and _Delegated Admin_
2. Review the result, and ensure this matches the proper mandate internally assigned in the organization.
3. If the organization grants different administrative rights to different users, you can further drill down into those rights granted to administrative users by returning to the Admin Console and reviewing the settings at **Admin Roles**

#### 2 Step Verification

Ensure that staff not only _can_ utilise Google's Two-Step Verification (two-factor authentication) login protection, but are also _enforced_ to do so.

1. From the **Security** section go to Basic Settings -> Two Step Verification
2. Ensure 'Allow users to turn on 2-step verification' is active.
3. Click 'Go to Advanced Settings to enforce 2-step verification'
4. Turn on enforcement of 2-Step Verification (2SV). G-Suite will not allow you to enable enforcement if you as the admin user do not have 2SV configured. Go to [https://www.google.com/landing/2step/](https://www.google.com/landing/2step/) in a new tab to configure 2SV for yourself before proceeding.
5. You have two enforcement options: Enabling it immediately will lock out users who do not have 2SV configured, and will require some manual support from the admin to log them in. It may be more convenient to _Turn on enforcement from date_ then set a date several days or a week in the future, and send out a notice to staff to configure 2SV before the deadline at [https://www.google.com/landing/2step/](https://www.google.com/landing/2step/).
6. It may be convenient to set a _New user enrolment period_ to give new users a short window to configure 2SV after their first succesful login.
7. If the organization operates in a region where a sophisticated nation state actor has privileged access to telecommunications, you may disable verification codes sent over SMS or phone calls by choosing the appropriate option under _Allowed 2-step verification methods_.

You can always use the 2SV Report (**Reports** -> Users -> Security) to see which users are currently enrolled in 2SV and which are not, in order to review compliance and target users who may need support or education.

#### Password Policies and Management

You may want to start from checking how passwords are currently used on organizational accounts by reviewing **Password Monitoring**, which will tell you the password length and approximate strength for each user. Next, review and set the minimum password requirements for your G-Suite domain:

1. From **Security** go to **Password Management**
2. Enable 'Enforce strong password'
3. Increase the minimal password length from 8 to a higher value (suggested: 15)
4. If you do not enforce the password policy at the next sign-in, then the new policy will not be applied until users will voluntarily change their passwords. Therefore, it is advisable to enforce the policy at the next sign-in. In this case, remember to inform the staff of this upcoming change in advance.
5. Decide whether or not to allow password re-use or to use password expiry.

#### Other Security Settings

Review the configuration of other sections under the **Security** page, including:

- **Activity Rules** - Make sure that alerts are active for relevant security-related events. The default settings here should generally be appropriate, but still make sure to review this in case alerts have been disabled.
- **Less Secure Apps** - Consider not allowing account access from less secure apps. Be aware this will block third-party applications such as Thunderbird email client, Adium and Pidgin chat clients, unless they have been configured with OAuth2.
- **Account Recovery** - Decide if self-service account recovery should be allowed. This can introduce the risk of account hacking. Most likely this should be off for Admin users as well as regular users.

Admins may want to restrict 3rd party access to organizational data by creating a whitelist of trusted services. Follow the Google documentation at [Control which third-party & internal apps access G Suite data](https://support.google.com/a/answer/7281227).

#### Other Indicators of Compromise
To view admin actions taken by any user with administrative rights, review the Admin Audit Log at **Reports** -> **Audit** -> **Admin**. Look for abnormal, unexplained, or unapproved changes. Use filters to narrow down the logs for easier review.

Other audit logs may be useful for security review, monitoring, and troubleshooting:

- **Login** logs will show user logins, their IP, the access type, if they failed to login, and if Google displayed a login challenge to them (2-Step).
- **Token** logs display 3rd party authorisations being granted and revoked by staff to allow access to their data.
- **User Accounts** logs display password changes and 2 Step Verification status changes
- **Drive** - Displays activity on Google Drive, which may be useful to track down unwanted external or internal sharing or unnecessary file access (access without 'need to know')

Check the **activity rules** under the **Security** page to ensure that important notifications/alerts have not been disabled.

Go to **Security** -> Advanced -> Authentication -> **Manage API client access** - review apps which have been granted access to user data
Go to **Security** -> API Permissions -> **App Access Control** -> **Manage third-party app access** - review apps which have been granted access to user data

#### G-Mail Hardening through Phishing and Malware Protection

Google offers additional protections against phishing and malware, in regards to permissible attachments, links in emails, and email sender authentication. These configurations are described in this article: [Advanced phishing and malware protection](https://support.google.com/a/answer/9157861?visit_id=637235775824583025-2730983772&amp;rd=1). Additional pre-delivery scanning to identify and divert malicious emails is also available as described at [Use enhanced pre-delivery message scanning](https://support.google.com/a/answer/7380368?hl=en)

These settings can be reviewed from the main G-Suite admin panel -> Apps -> G-Suite -> Gmail then under the sections marked **Safety** and **Advanced Settings**. For high-risk organizations it is most likely advisable to enable all or most of these optional protections and revisit them in case users start to experience deliverability issues.


#### Review unplanned exceptions through group policies and organizational units

In G-Suite, policies can bet set to apply exclusively to certain organizational units or to certain groups. Sometimes this measure is adopted by administrators to reduce control levels for certain users or accounts such as service accounts needed for external apps. This may lead to unintended gaps in the security controls where application of group or organizational unit policies are not done carefully. So, for each of the areas mentioned in this guide, also check if certain groups or organizational units have configurations which are not inherited from the main domain settings.

#### Mobile Device Management and Endpoint Sync

Mobile Device Management from G-Suite will allow you to ensure that staff devices accessing organizational communications and data meet a minimal security baseline. For instance, you can disallow devices which do not have a screen lock password or device encryption. In G-Suite domains opened after mid-2018, basic mobile device management may be enabled by default.

Endpoint Sync utilises an agent on the device accessing G-Suite applications to identify and authorize the endpoint device being used (ie. laptop or desktop computers). This is a useful way to build an asset inventory of staff devices, and to monitor devices which are lacking essential protection and updates as well as unauthorised devices accessing account data.

These are two powerful sets of security functionality which can assist in monitoring and managing the security of the organization. Their configuration is out of the scope of this article, but full documentation is available at [Overview: Manage devices with Google endpoint management](https://support.google.com/a/answer/1734200?hl=en)

#### Other Best Practices

Organizations should make sure their users with Super Admin roles do not get locked out of their own domain. Ideally, there would be more than one Super User, and each Super User would have updated, correct, and secured recovery methods set. In case all Super Users are locked out, they will need access to their domain's DNS records to verify ownership with Google Support.

### Comments

* See Google's advice on [Security checklist for small businesses (1-100 users](https://support.google.com/a/answer/9211704) and [Security checklist for medium and large businesses (100+ users)](https://support.google.com/a/answer/7587183?)
* Additional tips (some out of date, article is from 2017) at [More Tips for Securing Your G Suite](https://medium.com/@longtermsec/more-tips-for-securing-your-g-suite-4d617bd04bc8)
