---
title: Force Tor Browser to use specific exit nodes
keywords: circumvention, anonymity, Tor, exit nodes, torrc
last_updated: August 5, 2019
tags: [anonymity_circumvention, articles]
summary: "There is a need to force Tor Browser to use specific exit node/s explicitly."
sidebar: mydoc_sidebar
permalink: 147-Tor_force_exit_nodes.html
folder: mydoc
conf: Public
lang: en
ref: Tor_force_exit_nodes
---


# Force Tor Browser to use specific exit nodes
## How to set Tor Browser to only use specific exit nodes or certain countries

### Problem

By default, Tor browser randomly chooses which exit node to use when browsing the internet. In some cases, you may need to use specific exit node/s when visiting certain website/s. Such cases might be related to testing, investigation, research, or maybe related to specific need to exit on a  certain country or region.


* * *


### Solution

It is possible to force Tor Browser to explicitly use specific exit node/s - either by IP, country code, or fingerprint.

**Please note that this is not a solution that we suggest to clients, as this would risk
deanonymizing them - if a client needs to connect from a specific IP or country,
we should suggest them using a VPN.**

The configuration is done by modifying the [`torrc`](https://2019.www.torproject.org/docs/faq.html.en#torrc) file. When you [download the Tor Browser package](www.torproject.org/download/download), it can be extracted to any directory of your choice. You can find the `torrc` file in the extracted package.

- If you installed Tor Browser on Windows or Linux, the `torrc` file is in the data directory, which is `Browser/TorBrowser/Data/Tor` inside your Tor Browser directory.

- If you're on macOS, the `torrc` file is in the data directory at `~/Library/Application Support/TorBrowser-Data/Tor`. To get to it, press cmd-shift-g while in Finder and copy/paste that directory into the box that appears.

Open the `torrc` file using any text editor. To define the exit node/s you want to use, you will need to add a line based on the examples below:

1. If you know exactly which exit node you would like to use, you can define it by adding its specific IP or identity fingerprint, as follows:

    - To specify an exit node by its IP address, add the following line to `torrc`, replacing the example IP address with the one you need:

            ExitNodes 176.10.99.202

    - To specify an exit node by its fingerprint, add the following line to `torrc`, replacing the example fingerprint with the one you need:

            ExitNodes 19B6F025B4580795FBD9F3ED3C6574CDAF979A2F

2. To set Tor Browser to only use exit nodes in a specific country, add the following line to the `torrc` file, replacing the "us" code with the code of the country you prefer.

        ExitNodes {us} StrictNodes 1

3. To set Tor Browser to use exit nodes in more than one country, you can add multiple country codes separated by commas:

        ExitNodes {ua},{ug},{ie} StrictNodes 1

You can find a list of country codes [here](https://b3rn3d.herokuapp.com/blog/2014/03/05/tor-country-codes)

Save the `torrc` file after editing it and launch the Tor Browser.

You can make sure you are using the exit nodes you selected by going to [https://check.torproject.org/](https://check.torproject.org/) and clicking on the "Relay Search" link: a page with details on your exit node will open, including the IP address and country of the node.


* * *


### Comments

Note that if you list too few nodes you can degrade functionality. For example, if none of the exits you list allows traffic on port 80 or 443, you won’t be able to browse the web.

Instead of forcing the use of specific exit nodes, you can also set Tor Browser to avoid using exit nodes in a country, or specific exit nodes, with the `ExcludeNodes` option.

Also see the section on the [ExitNodes option in the Tor Manual](https://2019.www.torproject.org/docs/tor-manual.html.en#ExitNodes).

You can search for a specific exit node on [Tor Atlas](https://atlas.torproject.org/) to find its IP address or fingerprint.


* * *

### Related Articles
