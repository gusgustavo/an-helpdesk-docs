---
title: Vetting Request - Encrypted_French
keywords: email templates, vetting, vetting process, partner
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "External Vetting, email to partner (only encrypted)"
sidebar: mydoc_sidebar
permalink: 391-Vetting_Request_-_Encrypted_French.html
folder: mydoc
conf: Public
ref: Vetting_Request-Encrypted_French
lang: fr
---


# Vetting Request -  Encrypted_French
## External Vetting, email to partner (only encrypted)

### Body

Che(è)r(e) {{ beneficiary name }},

Je m'appelle {{ incident handler name }} et je fais partie de la ligne d'assistance Access Now Digital Security Helpline. J'ai reçu vos coordonnées de la part de [[ Nom de l'employé ou du partenaire de Access Now (pas les informations du client) ]]. Je vous contacte afin de demander de l’aide concernant le processus de vérification d’un.e nouveau/nouvelle client.e de la ligne d’assistance.

Récemment, nous avons reçu une demande d'assistance de [[ XXXX nom @ domaine, [[ Signal # ]], Poste, Organisation ]]. Iel demande nos conseils pour un problème de sécurité numérique.
Nous serions ravis de continuer à fournir notre assistance, mais nous devons d'abord vérifier auprès de partenaires de confiance leur identité et leur travail. Ce serait d'une grande aide pour nous si vous pouviez vérifier leur identité et confirmer qu'iel est un.e membre de confiance de la société civile.

Pouvez-vous confirmer que:

- Ces informations de contact sont associées à la personne en question
- Vous faites confiance à cette personne en général et pensez que ses actions n'affectent pas négativement la société civile

Si vous avez des questions ou des préoccupations, veuillez nous en informer. J'apprécie à l'avance votre aimable aide.

{{ incident handler name }}
