---
title: How to Circumvent the Great Firewall in China
keywords: circumvention, Great Firewall, China, VPN, Tor, censorship
last_updated: July 19, 2018
tags: [anonymity_circumvention, articles]
summary: "Client needs to overcome the Great Firewall in Mainland China"
sidebar: mydoc_sidebar
permalink: 110-VPN_China.html
folder: mydoc
conf: Public
lang: en
---


# How to Circumvent the Great Firewall in China
## VPN recommendations for Mainland China

### Problem

A client needs to circumvent the Great Firewall, either because they live, or are travelling to, Mainland China, but:

- VPN servers are frequently blocked in Mainland China.
- There is some evidence that the Great Firewall has the ability to selectively block VPN traffic.
- Some VPN providers do not respect users' privacy.
- Tor is blocked in Mainland China.

* * *


### Solution

#### VPNs

The solution we propose to the client will depend on what they need.

If the user is just interested in circumvention, they can use a VPN.

Sometimes some of these tools will work in some regions or circumstances and not
in others, and some of them may be blocked at the ISP level, so the best
strategy is to set up several VPNs before the client travels to China, so that
they have more than one option in case one doesn't work.

A good resource to identify available and functional VPN services in
China is [Great Fire](https://cc.greatfire.org/en).

From recent reports, we have seen that [AirVPN](https://airvpn.org/) works in
Beijing and [uProxy](https://www.uproxy.org/) and [ExpressVPN](https://www.expressvpn.com) work in Shanghai.

Recently we've also been informed that TunnelBear doesn't work in Shanghai.

We can offer the client free accounts for AirVPN and uProxy.

#### Tor

In China direct connections to the Tor network are blocked. To circumvent the Great Firewall, it is necessary to use a Tor bridge. A howto for using Tor bridges in China can be found
[here](https://support.torproject.org/censorship/connecting-from-china/).

The client should preferably install Tor Browser before they travel, since the Tor Project
website is blocked in China. Or they will need to use a service like [GetTor](https://gettor.torproject.org/).

- Article on what to do when [Tor is blocked in a
  country](https://blog.torproject.org/blog/breaking-through-censorship-barriers-even-when-tor-blocked)
  - [How to add bridges in the Tor Browser](https://tb-manual.torproject.org/bridges/)
  - [FAQ on Tor censorship](https://support.torproject.org/censorship/)


* * *


### Comments

As of October 2017, AirVPN worked for phones in Beijing.  Several
circumvention tools (e.g. Lantern, ShadowSocks, ShadowSocks R, Psiphon, Tor) are
likely to be blocked in certain networks. vyprvpn (free version) and
AnchorFree (free version) worked in Beijing with a DSL broadband provided by
either China Telecom or a local provider, but not with video streaming, just for
emails and instant messaging.


* * *

### Related Articles

- [Article #175: FAQ - Circumvention &amp; Anonymity tools](175-Circumvention_Anonymity_tools_list.html)
