---
title: Client Receives a Suspicious/Phishing Email
keywords: phishing, spearphishing, email, malware
last_updated: August 8, 2019
tags: [phishing_suspicious_email, articles]
summary: "A client has received an email with or without an attachment from a known or unknown source"
sidebar: mydoc_sidebar
permalink: 58-Suspicious_Phishing_Email.html
folder: mydoc
conf: Public
lang: en
ref: Suspicious_Phishing_Email
---


# Client Receives a Suspicious/Phishing Email 
## How to respond to phishing or to a suspicious email

### Problem

The email may contain malware or a link to a malicious site that tricks the
client into disclosing personal information. 

The longer a malevolent website stays online, the more victims it will create.
Reporting this website is one of the most important steps the Helpline can take.


* * *


### Solution

1. Reply to the client using the template in [Article #57: Phishing - First Email](57-Phishing-First_Email.html) to respond to their concerns and ask for additional
information that may be missing.

    Be sure to clearly state the email *should not be opened, nor a linked site
visited*.

    NOTE: If the client has already visited a site, or opened an attachment, we
should increase the urgency and impact of the case.


3. After receiving the headers and full email source from the client in Step 1, analyze the following:

    -  Use this [header analysis tool](https://toolbox.googleapps.com/apps/messageheader/). Did the headers pass SPF, DKIM, and DMARC authentication?

    - If only SPF and/or DKIM are authenticated, check the [headers manually](https://dmarcly.com/blog/home/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#identifier-alignment). If at least one of the following is true, we can consider the e-mail as not spoofed:
        - It passes SPF authentication, and [SPF has identifier alignment](https://dmarcly.com/blog/home/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#spf-identifier-alignment) (the domain portion of the envelope from address is aligned with the domain found in the header from address)
        - It passes DKIM authentication, and [DKIM has identifier alignment](https://dmarcly.com/blog/home/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#dkim-identifier-alignment) (the domain value in the d= field of the DKIM-signature in the email header is aligned with the domain found in the header from address)

    - Where is the email coming from? From what country, ISP, IP address?

    - Is the IP address part of the Tor network? You can check in the [Tor Atlas](https://metrics.torproject.org/rs.html) or in [this list](https://www.dan.me.uk/tornodes)

    - Are there any links in the email? If so, see [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html#5-analyze-suspicious-urls)

    - Are there any attachments? If so, see [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html#4-analyze-suspicious-files)

    - If the suspicious email was identified as Gmail scam, please read [these instructions](https://support.google.com/mail/answer/1074268?hl=en)


4. Once you have gathered all the information you need on the phishing message, search for the detected indicators of compromise on MISP, following the instructions in [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html).

5. Communicate to the client the conclusions of your investigation, and if necessary, [report the website](219-Targeted_Malware_Disable_Malicious_Server.html).

6. Add the event to MISP following the instructions in [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html).
* * *


### Comments

* [Use this infographic as a quick reference for the client to better understand phishing](https://phishme.com/project/how-to-spot-a-phish/)

* [This EFF guide is also useful](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)

* [Another useful reference on phishing by Security without Borders](https://guides.securitywithoutborders.org/guide-to-phishing/)
* * *


### Related Articles

- [Article #57: Phishing - First Email](57-Phishing-First_Email.html)
- [Article #209: Template - Phishing Link](209-Phishing_Link.html)
- [Article #219: Report and Disable Malicious C&amp;C Server](219-Disable_Malicious_Server.html)
- [Article #281: How to Recognize Spear-Phishing and What to Do](281-spear-phishing.html)
- [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html)
- [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html)
